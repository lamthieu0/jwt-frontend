import React, { useState } from "react";
import "./Register.scss";
import { useNavigate } from "react-router-dom";
import { registerNewUser } from "../../services/userService";
import { toast } from "react-toastify";

const Register = () => {
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const defaultInputValue = {
    isValidEmail: true,
    isValidPhone: true,
    isValidPassword: true,
    isValidUsername: true,
    isValidConfirmPassword: true,
  };

  const [objCheckInput, setObjCheckInput] = useState(defaultInputValue);

  const navigate = useNavigate();

  // useEffect(() => {
  //   axios.get("http://localhost:8081/api/v1/test-api").then((res) => {
  //     console.log(res.data);
  //   });
  // }, []);
  const handleLogin = () => {
    navigate("/login");
  };

  const handleRegister = async () => {
    let check = isValidInput();
    let userData = {
      email,
      phone,
      password,
      username,
    };

    if (check === true) {
      let resUser = await registerNewUser(userData);

      let dataUser = resUser.data;

      if (dataUser.code === 0) {
        toast.success(dataUser.message);
        navigate("/login");
      } else {
        toast.error(dataUser.message);
      }

      console.log("resUser", resUser);
    }
  };

  const isValidInput = () => {
    setObjCheckInput(defaultInputValue);
    if (!email) {
      setObjCheckInput({ ...defaultInputValue, isValidEmail: false });
      return false;
    }

    if (!phone) {
      setObjCheckInput({ ...defaultInputValue, isValidPhone: false });
      return false;
    }

    if (!password) {
      setObjCheckInput({ ...defaultInputValue, isValidPassword: false });
      return false;
    }

    if (password !== confirmPassword) {
      setObjCheckInput({
        ...defaultInputValue,
        isValidConfirmPassword: false,
      });
      return false;
    }

    // const emailRegex = /S+@S+.S+/;
    // if (!emailRegex.test(email)) {
    //   setObjCheckInput({ ...defaultInputValue, isValidEmail: false });
    //   return false;
    // }

    return true;
  };
  return (
    <div className="register-container">
      <div className="container">
        <div className="row px-3 px-sm-0">
          <div className="content-left col-12 d-none col-sm-7 d-sm-block">
            <div className="brand">Facebook</div>
            <div className="detail">Hello</div>
          </div>
          <div className="content-right col-sm-5 col-12 d-flex flex-column gap-3 py-3">
            <div className="brand d-sm-none">Facebook</div>
            <div className="form-group">
              <label htmlFor="email">Email:</label>
              <input
                required
                id="email"
                type="text"
                className={
                  objCheckInput.isValidEmail
                    ? "form-control"
                    : "form-control is-invalid"
                }
                placeholder="Email address"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="phone">Phone:</label>
              <input
                id="phone"
                type="text"
                className={
                  objCheckInput.isValidPhone
                    ? "form-control"
                    : "form-control is-invalid"
                }
                placeholder="Phone number"
                value={phone}
                onChange={(e) => setPhone(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="username">UserName:</label>
              <input
                id="username"
                type="text"
                className="form-control"
                placeholder="Username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password:</label>
              <input
                id="password"
                type="password"
                className={
                  objCheckInput.isValidPassword
                    ? "form-control"
                    : "form-control is-invalid"
                }
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="re-enter password">Re-enter password:</label>
              <input
                id="re-enter password"
                type="password"
                className={
                  objCheckInput.isValidConfirmPassword
                    ? "form-control"
                    : "form-control is-invalid"
                }
                placeholder="Re-enter password"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
            </div>

            <button
              className="btn btn-primary"
              type="submit"
              onClick={handleRegister}
            >
              Register
            </button>
            <hr />
            <div className="text-center">
              <button className="btn btn-success" onClick={handleLogin}>
                Already've an account. Login
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
