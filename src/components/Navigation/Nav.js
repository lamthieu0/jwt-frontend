import React, { useEffect, useState } from "react";
import "./Nav.scss";
import { NavLink, useLocation } from "react-router-dom";

const Nav = () => {
  const [isShow, setIsShow] = useState(false);

  const auth = sessionStorage.getItem("account");

  const location = useLocation();

  useEffect(() => {
    if (location.pathname === "/login" || !auth) {
      setIsShow(false);
    } else {
      setIsShow(true);
    }
  }, [auth, location.pathname]);

  return (
    isShow && (
      <div className="topnav">
        <NavLink to="/" exact>
          Home
        </NavLink>
        <NavLink to="/users">Users</NavLink>
        <NavLink to="/project">Projects</NavLink>
      </div>
    )
  );
};

export default Nav;
