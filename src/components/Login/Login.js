import React, { useEffect, useState } from "react";
import "./Login.scss";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { loginUser } from "../../services/userService";
import { history } from "../../services/history";

const Login = () => {
  const [valueLogin, setValueLogin] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    let session = sessionStorage.getItem("account");
    if (session) {
      history.push("/");
      window.location.reload();
    }
  }, []);

  const defaultObjValidInput = {
    isValidPassword: true,
    isValidValueLogin: true,
  };

  const [objValidInput, setObjValidInput] = useState(defaultObjValidInput);

  const navigate = useNavigate();
  const handleCreateAccount = () => {
    navigate("/register");
  };

  const handleLogin = async () => {
    let userData = {
      valueLogin,
      password,
    };

    setObjValidInput(defaultObjValidInput);

    if (!valueLogin) {
      toast.error("Please enter your email address or phone number");
      setObjValidInput({ ...defaultObjValidInput, isValidValueLogin: false });
      return;
    }
    if (!password) {
      toast.error("Please enter your password");
      setObjValidInput({ ...defaultObjValidInput, isValidPassword: false });
      return;
    }

    let response = await loginUser(userData);

    if (response && response.data && response.data.code === 0) {
      let data = {
        isAuthenticated: true,
        toke: "fake_token",
      };

      sessionStorage.setItem("account", JSON.stringify(data));
      toast.success(response.data.message);
      navigate("/users");
      // window.location.reload();
      // redux
    }

    if (response && response.data && response.data.code !== 0) {
      toast.error(response.data.message);
    }

    console.log("user", response.data);
  };
  return (
    <div className="login-container">
      <div className="container">
        <div className="row px-3 px-sm-0">
          <div className="content-left col-12 d-none col-sm-7 d-sm-block">
            <div className="brand">Facebook</div>
            <div className="detail">Hello</div>
          </div>
          <div className="content-right col-sm-5 col-12 d-flex flex-column gap-3 py-3">
            <div className="brand d-sm-none">Facebook</div>
            <input
              type="text"
              className={
                objValidInput.isValidValueLogin
                  ? "form-control"
                  : "form-control is-invalid"
              }
              placeholder="Email or phone number"
              value={valueLogin}
              onChange={(e) => setValueLogin(e.target.value)}
            />
            <input
              type="password"
              className={
                objValidInput.isValidPassword
                  ? "form-control"
                  : "form-control is-invalid"
              }
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              onKeyDown={(e) => {
                if (e.key === "Enter") {
                  handleLogin();
                }
              }}
            />
            <button className="btn btn-primary" onClick={handleLogin}>
              Login
            </button>
            <span className="text-center">
              <a href="/#" className="forgot-password">
                Forgot your password?
              </a>
            </span>
            <hr />
            <div className="text-center">
              <button className="btn btn-success" onClick={handleCreateAccount}>
                Create new account
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
