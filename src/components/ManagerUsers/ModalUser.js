import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { groupUser, createUser } from "../../services/userService";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import _ from "lodash";

const ModalUser = (props) => {
  const { setIsShowModalCreate, fetchUsers, isShowModalCreate } = props;
  /**
   * Khai báo biến defaultUserData để lưu cho từng giá trị của form
   * */
  const defaultUserData = {
    email: "",
    phone: "",
    password: "",
    username: "",
    address: "",
    gender: "",
    userGroup: "",
  };

  const validInputsDefault = {
    email: true,
    phone: true,
    password: true,
    username: true,
    address: true,
    gender: true,
    userGroup: true,
  };

  const [userData, setUserData] = useState(defaultUserData);
  const [validInput, setValidInput] = useState(validInputsDefault);
  const [group, setGroup] = useState([]);

  const onHideModalUser = () => {
    setIsShowModalCreate(false);
    setUserData(defaultUserData);
  };

  const onShowModalUser = () => {
    setIsShowModalCreate(true);
  };

  useEffect(() => {
    getGroup();
  }, []);

  const getGroup = async () => {
    let result = await groupUser();
    if (result.data && result.data.code === 0) {
      setGroup(result.data.data);

      /**
       * set default value for userGroup
       */
      if (result.data.data && result.data.data.length > 0) {
        let groups = result.data.data;
        setUserData({
          ...userData,
          userGroup: groups[0].id,
        });
      }
    } else {
      toast.error(result.data.message);
    }
  };

  const handleOnChangeInput = (value, name) => {
    /**
     * Sử dụng cloneDeep của lodash để clone lại giá trị của state userData
     *
     */
    let _userData = _.cloneDeep(userData);
    _userData[name] = value;
    setUserData(_userData);
  };

  const checkValidInputs = () => {
    setValidInput(validInputsDefault);
    let arr = ["email", "phone", "password", "userGroup"];

    let check = true;

    for (let i = 0; i < arr.length; i++) {
      if (!userData[arr[i]]) {
        /**
         * Update array validInput
         */
        let _validInputs = _.cloneDeep(validInputsDefault);
        _validInputs[arr[i]] = false;
        setValidInput(_validInputs);

        toast.error(`Empty field ${arr[i]}`);
        check = false;
        break;
      }
    }

    return check;
  };

  const handleConfirmUser = async () => {
    let check = checkValidInputs();

    if (check === true) {
      let res = await createUser({
        ...userData,
        groupId: userData["userGroup"],
      });

      if (res.data && res.data.code === 0) {
        toast.success(res.data.message);
        onHideModalUser();
        setUserData({ ...defaultUserData, groupId: group[0].id });
        await fetchUsers();
      }
      if (res.data && res.data.code !== 0) {
        let _validInputs = _.cloneDeep(validInputsDefault);
        _validInputs[res.data.data] = false;
        setValidInput(_validInputs);
        toast.error(res.data.message);
      }
    }
  };

  const handleRefresh = async () => {
    await fetchUsers();
  };

  return (
    <>
      <div className="actions my-2">
        <button className="btn btn-success me-2" onClick={handleRefresh}>
          Refresh
        </button>
        <button className="btn btn-primary" onClick={onShowModalUser}>
          Add new user
        </button>
      </div>
      <Modal size="lg" show={isShowModalCreate} className="modal-user">
        <Modal.Header closeButton onHide={onHideModalUser}>
          <Modal.Title id="contained-modal-title-vcenter">
            <span>Add new user</span>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="content-body row">
            <div className="col-12 col-sm-6 form-group">
              <label>
                Email address: (<span className="red">*</span>)
              </label>
              <input
                className={
                  validInput.email ? "form-control" : "form-control is-invalid"
                }
                type="email"
                value={userData.email}
                onChange={(e) => {
                  handleOnChangeInput(e.target.value, "email");
                }}
              />
            </div>
            <div className="col-12 col-sm-6 form-group">
              <label>
                Phone Number: (<span className="red">*</span>)
              </label>
              <input
                className={
                  validInput.phone ? "form-control" : "form-control is-invalid"
                }
                type="text"
                value={userData.phone}
                onChange={(e) => {
                  handleOnChangeInput(e.target.value, "phone");
                }}
              />
            </div>
            <div className="col-12 col-sm-6 form-group">
              <label>Username: </label>
              <input
                className="form-control"
                type="text"
                value={userData.username}
                onChange={(e) => {
                  handleOnChangeInput(e.target.value, "username");
                }}
              />
            </div>
            <div className="col-12 col-sm-6 form-group">
              <label>
                Password: (<span className="red">*</span>)
              </label>
              <input
                className={
                  validInput.password
                    ? "form-control"
                    : "form-control is-invalid"
                }
                type="password"
                value={userData.password}
                onChange={(e) => {
                  handleOnChangeInput(e.target.value, "password");
                }}
              />
            </div>
            <div className="col-12 col-sm-12 form-group">
              <label>Address: </label>
              <input
                className="form-control"
                type="text"
                value={userData.address}
                onChange={(e) => {
                  handleOnChangeInput(e.target.value, "address");
                }}
              />
            </div>
            <div className="col-12 col-sm-6 form-group">
              <label>Gender: </label>
              <select
                className="form-select"
                onChange={(e) => handleOnChangeInput(e.target.value, "gender")}
              >
                <option selected value={"Male"}>
                  Male
                </option>
                <option value={"Female"}>Female</option>
                <option value={"Other"}>Other</option>
              </select>
            </div>
            <div className="col-12 col-sm-6 form-group">
              <label>
                Group (<span className="red">*</span>) :
              </label>
              <select
                className={
                  validInput.userGroup
                    ? "form-select"
                    : "form-select is-invalid"
                }
                onChange={(e) =>
                  handleOnChangeInput(e.target.value, "userGroup")
                }
              >
                {group.map((item, index) => {
                  return (
                    <option key={`group-${index}`} value={item.id}>
                      {item.name}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onHideModalUser}>
            Close
          </Button>
          <Button variant="primary" onClick={handleConfirmUser}>
            Save
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalUser;
