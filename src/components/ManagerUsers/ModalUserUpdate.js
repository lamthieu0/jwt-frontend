import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { groupUser, updateUser } from "../../services/userService";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import _ from "lodash";

const ModalUserUpdate = (props) => {
  const {
    setIsShowModalUpdate,
    dataModalUpdate,
    isShowModalUpdate,
    fetchUsers,
  } = props;
  /**
   * Khai báo biến defaultUserData để lưu cho từng giá trị của form
   * */

  const validInputsDefault = {
    email: true,
    phone: true,
    password: true,
    username: true,
    address: true,
    gender: true,
    userGroup: true,
  };

  const [userData, setUserData] = useState(dataModalUpdate);

  const [validInput, setValidInput] = useState(validInputsDefault);
  const [group, setGroup] = useState([]);

  useEffect(() => {
    if (dataModalUpdate) {
      setUserData(dataModalUpdate);
    }
  }, [dataModalUpdate]);

  const onHideModaUpdatelUser = () => {
    setIsShowModalUpdate(false);
    setUserData(dataModalUpdate);
  };

  useEffect(() => {
    getGroup();
  }, []);

  const getGroup = async () => {
    let result = await groupUser();
    if (result.data && result.data.code === 0) {
      setGroup(result.data.data);
      if (result.data.data && result.data.data.length > 0) {
        setUserData({
          ...userData,
          userGroup: userData.userGroup,
        });
      }
    } else {
      toast.error(result.data.message);
    }
  };

  const handleOnChangeInput = (value, name) => {
    let _userData = _.cloneDeep(userData);
    _userData[name] = value;
    setUserData(_userData);
  };

  const handleConfirmUpdateUser = async () => {
    let res = await updateUser({
      ...userData,
      groupId: userData.groupId,
    });

    if (res.data && res.data.code === 0) {
      toast.success(res.data.message);
      onHideModaUpdatelUser();
      await fetchUsers();
    }
    if (res.data && res.data.code !== 0) {
      let _validInputs = _.cloneDeep(validInputsDefault);
      _validInputs[res.data.data] = false;
      setValidInput(_validInputs);
      toast.error(res.data.message);
    }
  };

  return (
    <>
      <Modal size="lg" show={isShowModalUpdate} className="modal-user">
        <Modal.Header closeButton onHide={onHideModaUpdatelUser}>
          <Modal.Title id="contained-modal-title-vcenter">
            <span>Edit a user</span>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="content-body row">
            <div className="col-12 col-sm-6 form-group">
              <label>
                Email address: (<span className="red">*</span>)
              </label>
              <input
                disabled
                className={
                  validInput.email ? "form-control" : "form-control is-invalid"
                }
                type="email"
                value={userData.email}
                onChange={(e) => {
                  handleOnChangeInput(e.target.value, "email");
                }}
              />
            </div>
            <div className="col-12 col-sm-6 form-group">
              <label>
                Phone Number: (<span className="red">*</span>)
              </label>
              <input
                disabled
                className={
                  validInput.phone ? "form-control" : "form-control is-invalid"
                }
                type="text"
                value={userData.phone}
                onChange={(e) => {
                  handleOnChangeInput(e.target.value, "phone");
                }}
              />
            </div>
            <div className="col-12 col-sm-6 form-group">
              <label>Username: </label>
              <input
                className="form-control"
                type="text"
                value={userData.username}
                onChange={(e) => {
                  handleOnChangeInput(e.target.value, "username");
                }}
              />
            </div>
            <div className="col-12 col-sm-12 form-group">
              <label>Address: </label>
              <input
                className="form-control"
                type="text"
                value={userData.address}
                onChange={(e) => {
                  handleOnChangeInput(e.target.value, "address");
                }}
              />
            </div>
            <div className="col-12 col-sm-6 form-group">
              <label>Gender: </label>
              <select
                className="form-select"
                onChange={(e) => handleOnChangeInput(e.target.value, "gender")}
                value={userData.gender}
              >
                <option value={"Male"}>Male</option>
                <option value={"Female"}>Female</option>
                <option value={"Other"}>Other</option>
              </select>
            </div>
            <div className="col-12 col-sm-6 form-group">
              <label>
                Group (<span className="red">*</span>) :
              </label>
              <select
                value={userData.groupId}
                className={
                  validInput.userGroup
                    ? "form-select"
                    : "form-select is-invalid"
                }
                onChange={(e) => handleOnChangeInput(e.target.value, "groupId")}
              >
                {group.map((item, index) => {
                  return (
                    <option key={`group-${index}`} value={item.id}>
                      {item.name}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onHideModaUpdatelUser}>
            Close
          </Button>
          <Button variant="primary" onClick={handleConfirmUpdateUser}>
            Update
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalUserUpdate;
