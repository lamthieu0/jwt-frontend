import "./Users.scss";
import { useEffect, useState } from "react";
import { fetchAllUser, deleteUser } from "../../services/userService";
import ReactPaginate from "react-paginate";
import { toast } from "react-toastify";
import ModalDelete from "./ModalDelete";
import ModalUser from "./ModalUser";
import ModalUserUpdate from "./ModalUserUpdate";

const Users = () => {
  const [listUser, setListUser] = useState([]);
  const [page, setPage] = useState(1);
  const [limit] = useState(1);
  const [totalPages, setTotalPages] = useState(0);

  // Modal delete
  const [isShowModalDelete, setIsShowModalDelete] = useState(false);
  const [dataModal, setDataModal] = useState({});

  //Action Modal create
  const [isShowModalCreate, setIsShowModalCreate] = useState(false);

  // Action Modal Update
  const [isShowModalUpdate, setIsShowModalUpdate] = useState(false);
  const [dataModalUpdate, setDataModalUpdate] = useState({});

  const fetchUsers = async () => {
    let result = await fetchAllUser(page, limit);
    if (result.data.data.users.length > 0) {
      setListUser(result.data.data.users);
      setTotalPages(result.data.data.totalPages);
    }
  };

  useEffect(() => {
    fetchUsers();
  }, [page]);

  const handlePageClick = async (event) => {
    setPage(+event.selected + 1);
  };

  const handleDeleteUser = async (user) => {
    setDataModal(user);
    setIsShowModalDelete(true);
  };

  const handleClose = () => {
    setIsShowModalDelete(false);
    setDataModal({});
  };

  const handleEditUser = (user) => {
    console.log("user", user);
    setDataModalUpdate(user);
    setIsShowModalUpdate(true);
  };

  const handleConfirm = async () => {
    let result = await deleteUser(dataModal);
    if (result && result.data.code === 0) {
      toast.success(result.data.message);
      setIsShowModalDelete(false);
      await fetchUsers();
    } else {
      setIsShowModalDelete(false);
      toast.error(result.data.message);
    }
  };

  return (
    <>
      <div className="container">
        <div className="manager-users-container">
          <div className="user-header">
            <div className="title">
              <h3>Table Users</h3>
            </div>
            <ModalUser
              fetchUsers={fetchUsers}
              isShowModalCreate={isShowModalCreate}
              setIsShowModalCreate={setIsShowModalCreate}
            />
            <ModalUserUpdate
              dataModalUpdate={dataModalUpdate}
              isShowModalUpdate={isShowModalUpdate}
              setIsShowModalUpdate={setIsShowModalUpdate}
              fetchUsers={fetchUsers}
            />
          </div>
          <div className="user-body">
            <table className="table table-bordered table-hover">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Id</th>
                  <th scope="col">Email</th>
                  <th scope="col">UserName</th>
                  <th scope="col">Group</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                {listUser && listUser.length > 0 ? (
                  <>
                    {listUser.map((item, index) => {
                      return (
                        <>
                          <tr key={`row-${index}`}>
                            <td>{(page - 1) * limit + index + 1}</td>
                            <td>{item.id}</td>
                            <td>{item.email}</td>
                            <td>{item.username}</td>
                            <td>{item.Group ? item.Group.name : ""}</td>
                            <td>
                              <button
                                className="btn btn-warning mx-3"
                                onClick={() => handleEditUser(item)}
                              >
                                Edit
                              </button>
                              <button
                                className="btn btn-danger"
                                onClick={() => handleDeleteUser(item)}
                              >
                                Delete
                              </button>
                            </td>
                          </tr>
                        </>
                      );
                    })}
                  </>
                ) : (
                  <>
                    <tr>
                      <td>
                        <span>404 Not Found</span>
                      </td>
                    </tr>
                  </>
                )}
              </tbody>
            </table>
          </div>
          {totalPages > 0 && (
            <div className="user-footer">
              <ReactPaginate
                nextLabel="next >"
                onPageChange={handlePageClick}
                pageRangeDisplayed={3}
                marginPagesDisplayed={2}
                pageCount={totalPages}
                previousLabel="< previous"
                pageClassName="page-item"
                pageLinkClassName="page-link"
                previousClassName="page-item"
                previousLinkClassName="page-link"
                nextClassName="page-item"
                nextLinkClassName="page-link"
                breakLabel="..."
                breakClassName="page-item"
                breakLinkClassName="page-link"
                containerClassName="pagination"
                activeClassName="active"
                renderOnZeroPageCount={null}
              />
            </div>
          )}
        </div>
      </div>
      <ModalDelete
        show={isShowModalDelete}
        handleClose={handleClose}
        handleConfirm={handleConfirm}
        dataModal={dataModal}
      />
    </>
  );
};

export default Users;
