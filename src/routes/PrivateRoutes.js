import { Navigate } from "react-router-dom";

const PrivateRoutes = ({ children }) => {
  const session = sessionStorage.getItem("account");

  return session ? <>{children}</> : <Navigate to={"/login"} />;
};

export default PrivateRoutes;
