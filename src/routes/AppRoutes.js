import { Routes, Route } from "react-router-dom";
import Login from "../components/Login/Login";
import Users from "../components/ManagerUsers/Users";
import Register from "../components/Register/Register";
import PrivateRoutes from "./PrivateRoutes";

const AppRoutes = (props) => {
  return (
    <Routes>
      <Route
        path="/users"
        element={
          <PrivateRoutes>
            <Users />
          </PrivateRoutes>
        }
      />
      <Route path="/login" element={<Login />} />
      <Route path="/register" element={<Register />} />
      <Route path="/" exact element={<div>Home</div>} />
      <Route path="*" element={<div>404 Not Found</div>} />
    </Routes>
  );
};

export default AppRoutes;
