import axios from "../config/axios";

const registerNewUser = (data) => {
  return axios.post("register", data);
};

const loginUser = (data) => {
  return axios.post("login", data);
};

const fetchAllUser = (page, limit) => {
  return axios.get(`user/read?page=${page}&limit=${limit}`);
};

const deleteUser = (user) => {
  return axios.delete("user/delete", {
    data: { id: user.id },
  });
};

const createUser = (userData) => {
  return axios.post("user/create", {
    ...userData,
  });
};

const groupUser = () => {
  return axios.get("group/read");
};

const updateUser = (userData) => {
  return axios.put("user/update", {
    ...userData,
  });
};

export {
  registerNewUser,
  loginUser,
  fetchAllUser,
  deleteUser,
  groupUser,
  createUser,
  updateUser,
};
